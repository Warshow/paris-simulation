package com.paris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParisH2Application {

	public static void main(String[] args) {
		SpringApplication.run(ParisH2Application.class, args);
	}
}
