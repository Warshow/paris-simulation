package com.paris.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.paris.exceptions.NotRealizedEventException;

@Entity
public class Evenement {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="idAction")
	private Action action;
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="idEntite")
	private Entite entite;
	@Column
	private double cote;
	@Column
	private boolean realise;
	@Column
	private Date date;
	@Column
	private String type;
	
	public Evenement(Action action, Entite entite,double cote) {
		this.action = action;
		this.entite = entite;
		this.cote=cote;
		this.realise=false;
		date=new Date();
	}
	
	public Evenement(Action action, Entite entite) {
		this(action,entite,1);
	}
	
	public Evenement() {
		this("","");
	}
	
	public Evenement(String action, String entite,double cote) {
		this(new Action(action),new Entite(entite),cote);
	}
	
	public Evenement(String action, String entite) {
		this(new Action(action),new Entite(entite),1);
	}

	public double getCote() {
		return cote;
	}

	public Evenement setCote(double cote) {
		this.cote = cote;
		return this;
	}

	public boolean estRealise() {
		return realise && estPasse();
	}

	public void setRealise(boolean realise) throws NotRealizedEventException{
		if(!estPasse()) {
			throw new NotRealizedEventException();
		}
		this.realise = realise;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Action getAction() {
		return action;
	}

	public Entite getEntite() {
		return entite;
	}
	
	public boolean estPasse() {
		Date now=new Date();
		return date.before(now);
	}
	
	@Override
	public String toString() {
		String strRealise="en attente r�alisation...";
		if(estPasse()) {
				strRealise=realise?"R�alis�":"Non r�alis�";
		}
		return entite.toString()+" "+action.toString()+" (cote : "+cote+") "+strRealise;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setEntite( Entite entite) {
		this.entite=entite;
		
	}

	public void setAction(Action action) {
		this.action=action;
		
	}

	
}
