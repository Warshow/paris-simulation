package com.paris.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;


@Entity
public class PariCombine extends Pari {

	@OneToMany(cascade=CascadeType.PERSIST)
	protected List<Evenement> evenements;
	
	public PariCombine(Joueur joueur, float mise) {
		super(joueur, mise);
		evenements=new ArrayList<>();
	}
	

	
	public void ajouterEvenements(Evenement...evenements) {
		for(Evenement event:evenements) {
			this.evenements.add(event);
		}
	}
	


	@Override
	public boolean estGagne() {
		// TODO Auto-generated method stub
		
		for(Evenement eve:evenements) {
			if(!eve.estRealise()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public double gains() {
		
		double gains=0;
		for(Evenement eve:evenements) {
			if(eve.estPasse() && eve.estRealise()) {
				gains=gains+mise*eve.getCote(); 
			}
		}
		return 0;
	}



	public void clearEvenements() {
		evenements.clear();
		
	}



	public Object nbEvenements() {
		return evenements.size();
	}



	@Override
	public List<Evenement> getEvenements() {
		return evenements;
	}



	@Override
	public String getType() {
		return "Combine";
	}

}
