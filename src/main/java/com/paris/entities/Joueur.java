package com.paris.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Joueur{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	

	@Column(name="nom")
	private String nom;
	@Column(name="prenom")
	private String prenom;
	@Column(name="age")
	private int age;
	@Column(name="identifiant")
	private String identifiant;
	@Column(name="password")
	private String password;

	public Joueur(String nom,String prenom,int age, String identifiant, String password) throws Exception {
		this.nom=nom;
		this.prenom=prenom;
		setAge(age);
		this.identifiant=identifiant;
		this.password=password;
	}
	
	
	
	public Joueur(String nom, String prenom,String identifiant, String password) throws Exception {
		this(nom,prenom,18,identifiant,password);
	}



	public Joueur() throws Exception {
		this("","",18,"idtest","passtest");
	}


	public String getIdentifiant() {
		return identifiant;
	}



	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) throws Exception {
		if(age>=18) {
			this.age = age;
		}else {
			throw new Exception("vous n'avez pas l'âge légal !");
		}
	}

	public void setNom(String nom) {
		this.nom=nom;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void afficher() {
		System.out.println(this);
	}
	
	public static void afficher(Joueur unJoueur) {
		unJoueur.afficher();
	}
	
	
	public String tostring() {
		return prenom+" "+this.nom;
	}
	public int getId() {
		return id;
	}

}
