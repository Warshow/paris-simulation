package com.paris.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paris.entities.Entite;
import com.paris.entities.Evenement;
import com.paris.entities.Joueur;
import com.paris.repositories.EventRepository;
import com.paris.repositories.JoueurRepository;

import io.github.jeemv.springboot.vuejs.VueJS;
import io.github.jeemv.springboot.vuejs.utilities.Http;

@Controller
@RequestMapping("")
public class IndexControllers {
	
	@Autowired
	private EventRepository repo;
	
	@Autowired
	private JoueurRepository repojoueur;
	
	@ModelAttribute("vue")
	public VueJS getVue() {
		return new VueJS("#app");
	}

	/*@PostMapping("/connexionJoueur")
	public boolean connect(HttpSession session) {
		Joueur membre =(Joueur) session.getAttribute("membre");
		if(membre==null) {
			return false;
		}else {
			return true;
		}
		
	}*/
	
	@RequestMapping("connexionJoueur")
	@ResponseBody
	public String con(HttpSession session, HttpServletRequest joueur) {
		Joueur optJoueur= repojoueur.findByIdentifiantAndPassword(joueur.getParameter("identifiant"),joueur.getParameter("password"));
		if(optJoueur.getNom().isEmpty()) {
			return "connexion";
		}else {
			session.setAttribute("membre", optJoueur);
			return "espacemembre/index";
		}
	}
	
	@GetMapping("")
	public String Index(@ModelAttribute("vue") VueJS vue) {
		List<Evenement> events=repo.findAll();
		
		vue.addData("events", events);
		return "index";
	}
	
	@GetMapping("/Connexion")
	public String Connexion(@ModelAttribute("vue") VueJS vue) {
		return "connexion";
	}
	
	
	@GetMapping("/Inscription")
	public String register(@ModelAttribute("vue") VueJS vue) throws Exception {
		vue.addData("joueur", new Joueur());
		vue.addMethod("submitForm", Http.submitForm("formJoueur", "this.$message({message: 'Inscription effectuer',type: 'success'});"));
		return "register";
	}
	@PostMapping("submitForm")
	@ResponseBody
	public String submit(@RequestBody Joueur joueur) throws Exception {
		repojoueur.saveAndFlush(joueur);
		return"espacemembre/index";
	}
	
	@GetMapping("/Profil")
	public String Profil(@ModelAttribute("vue") VueJS vue) throws Exception {
		return "espacemembre/profil";
	}
}
