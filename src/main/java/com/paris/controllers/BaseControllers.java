package com.paris.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paris.entities.Joueur;
import com.paris.repositories.JoueurRepository;

import io.github.jeemv.springboot.vuejs.VueJS;

@Controller
@RequestMapping("/espacemembre/")
public class BaseControllers {
	
	@Autowired
	private JoueurRepository repo;
	
	@ModelAttribute("vue")
	public VueJS getVue() {
		return new VueJS("#app");
	}



	@GetMapping("")
	public String TableauBord(@ModelAttribute("vue") VueJS vue) {
		return "espacemembre/index";
	}
	
	@GetMapping("test")
	public String Index(@ModelAttribute("vue") VueJS vue) {
		return "nologin/index";
	}
	
	
	
}
