package com.paris.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.paris.entities.Pari;

@Repository
public interface PariRepository extends JpaRepository<Pari, Integer> {

}
