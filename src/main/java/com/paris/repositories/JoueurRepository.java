package com.paris.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.paris.entities.Joueur;

@Repository
public interface JoueurRepository extends JpaRepository<Joueur, Integer> {

	Joueur findByIdentifiantAndPassword(String identifiant, String password);
	

}


