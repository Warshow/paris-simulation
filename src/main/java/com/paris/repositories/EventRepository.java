package com.paris.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.paris.entities.Evenement;

@Repository
public interface EventRepository extends JpaRepository<Evenement, Integer> {

}